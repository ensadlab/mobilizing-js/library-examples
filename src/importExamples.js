
//automatically generated with src/bin/import-examples.js script on 28/1/2021 at 21:56.1
const index = {};
import * as main from "./main.js";
export { main };
index.main = main;

import * as audio_source_3D from "./examples/audio/audio_source_3D/script.js";
export { audio_source_3D };
index.audio_source_3D = audio_source_3D;

import * as listener_3D from "./examples/audio/listener_3D/script.js";
export { listener_3D };
index.listener_3D = listener_3D;

import * as listener_3D_gyro from "./examples/audio/listener_3D_gyro/script.js";
export { listener_3D_gyro };
index.listener_3D_gyro = listener_3D_gyro;

import * as listener_3D_mouse from "./examples/audio/listener_3D_mouse/script.js";
export { listener_3D_mouse };
index.listener_3D_mouse = listener_3D_mouse;

import * as accel_gravity from "./examples/input/accel_gravity/script.js";
export { accel_gravity };
index.accel_gravity = accel_gravity;

import * as IIWU from "./examples/light/IIWU/script.js";
export { IIWU };
index.IIWU = IIWU;

import * as custom_shader from "./examples/material/custom_shader/script.js";
export { custom_shader };
index.custom_shader = custom_shader;

import * as introduction from "./examples/material/introduction/script.js";
export { introduction };
index.introduction = introduction;

import * as sprite from "./examples/material/sprite/script.js";
export { sprite };
index.sprite = sprite;

import * as texture from "./examples/material/texture/script.js";
export { texture };
index.texture = texture;

import * as three_material from "./examples/material/three_material/script.js";
export { three_material };
index.three_material = three_material;

import * as box from "./examples/shapes/box/script.js";
export { box };
index.box = box;

import * as three_geometry from "./examples/shapes/three_geometry/script.js";
export { three_geometry };
index.three_geometry = three_geometry;

import * as text_3D from "./examples/text/text_3D/script.js";
export { text_3D };
index.text_3D = text_3D;

import * as button from "./examples/UI/button/script.js";
export { button };
index.button = button;

import * as input_field from "./examples/UI/input_field/script.js";
export { input_field };
index.input_field = input_field;

import * as keyboard from "./examples/UI/keyboard/script.js";
export { keyboard };
index.keyboard = keyboard;

export default index;
