import * as Mobilizing from '@mobilizing/library';

export class Script {

    constructor() {
        const text = "L’apparition des smartphones et des tablettes dans la dernière moitié des années 2000 a donné lieu à leur adoption massive les écrans mobiles, hybridation du téléphone portable et des ordinateurs, ont su se rendre indispensables et peuplent aujourd’hui notre quotidien. Étant une forme d’ordinateur adaptée à des situations de mobilité, la nouveauté de ces machines numériques s’est progressivement diluée dans le paysage technologique contemporain : elles font maintenant partie intégrante de l’ensemble technique numérique qui caractérise notre époque. Cette dilution ne correspond cependant pas à une uniformisation des dispositifs numériques qui permettraient de remplacer l’un par l’autre indifféremment. Au contraire, elle résulte d’une nécessité d’interopérabilité entre les différents formats des appareils dont nous nous entourons. En effet, les écrans mobiles présentent des spécificités techniques qui leur sont propres et qu’il convient d’explorer au-delà de leurs usages préconçus. Les champs du design et des pratiques artistiques de l’interactivité sont particulièrement propices à cette exploration. Parce qu’ils permettent d’obtenir des informations concernant leur état physique propre (orientation, inclinaison, etc.), les écrans mobiles annulent le besoin d’interfaces de contrôle tierces, tels que la souris ou le clavier. A l’aide des nombreux capteurs qu’ils embarquent, les écrans mobiles permettent d’interagir avec les médias qu’ils diffusent (image, textes, vidéos, etc.) à travers leur manipulation : ils sont leur propre périphérique de contrôle. Pendant un temps, la mise en œuvre exploratoire et artistique de ces spécificités techniques s’est avérée difficile d’accès, les conditions pratiques de la création d’ouvrages interactifs sur les écrans mobiles demandant la maîtrise d’une chaîne de développement conçue pour les spécialistes de l’informatique. Mais cette situation tend à se transformer."

        this.words = text.split(" ");
    }

    preLoad(loader) {
        this.fontRequest = loader.loadArrayBuffer({ "url": "./assets/fonts/Raleway-Regular.ttf" });
    }

    setup() {
        this.renderer = new Mobilizing.three.RendererThree({ "antialias": true });
        this.context.addComponent(this.renderer);

        this.camera = new Mobilizing.three.Camera();
        this.camera.setFOV(30);
        this.camera.setToPixel(); //adapt automatically the camera to the window
        this.renderer.addCamera(this.camera);

        //inputs
        const touch = this.context.addComponent(new Mobilizing.input.Touch({ "target": this.renderer.canvas }));
        touch.setup();//set it up
        touch.on();//active it

        const mouse = this.context.addComponent(new Mobilizing.input.Mouse({ "target": this.renderer.canvas }));
        mouse.setup();//set it up
        mouse.on();//active it

        this.pointer = new Mobilizing.input.Pointer();
        this.context.addComponent(this.pointer);
        this.pointer.add(touch);
        this.pointer.add(mouse);
        this.pointer.setup();//set it up
        this.pointer.on();//active it

        //scene construction
        const size = this.renderer.getCanvasSize();

        this.light = new Mobilizing.three.Light({ type: "spot" });
        this.light.transform.setLocalPosition(size.width / 2, -size.height / 2, 1000);
        this.light.setTargetPosition(size.width / 2, -size.height / 2, 0);
        this.light.setAngle(Math.PI / 30);
        this.light.setPenumbra(.5);
        this.light.setIntensity(1.5);
        this.renderer.addToCurrentScene(this.light);

        //Build a Node, that is an empty object that can be used to create groups
        //a node has no geometry but can have many children, grouped together
        this.node = new Mobilizing.three.Node();
        this.renderer.addToCurrentScene(this.node);

        const typeface = this.fontRequest.getValue();

        for (let i = 0; i < this.words.length; i++) {

            this.callMake3DText(typeface, this.words[i]).then((value) => {
                console.log(value);

                const txt = value;
                txt.transform.setLocalPosition(
                    Mobilizing.math.randomFromTo(0, size.width),
                    Mobilizing.math.randomFromTo(0, -size.height)
                );
                this.node.transform.addChild(txt.transform);

            });
        }
    }

    make3DText(typeface, text) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(new Mobilizing.three.TextOpentype({
                    "fontFile": typeface,
                    "text": text,
                    "depth": 0,
                    "fontSize": 20,
                    //"material": "basic"
                }));
            }, 1000);
        });

    }

    async callMake3DText(typeface, text) {
        const result = await this.make3DText(typeface, text);
        return result;
    }

    update() {
        if (this.pointer.getState()) {
            this.light.transform.setLocalPosition(this.pointer.getX(), - this.pointer.getY());
            let lightPos = this.light.transform.getLocalPosition();
            this.light.setTargetPosition(lightPos.x, lightPos.y, 0);
        }
    }
}
