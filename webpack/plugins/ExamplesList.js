const path = require('path');
const fs = require('fs');
const VirtualModulesPlugin = require('webpack-virtual-modules');
const { NormalModule } = require('webpack');

const PLUGIN_ID = 'ExamplesListPlugin';

module.exports = class ExamplesListPlugin {
  constructor(options) {
    this.options = options;

    this.needs_compilation = true;
  }

  apply(compiler) {
    const virtual_modules = new VirtualModulesPlugin();
    virtual_modules.apply(compiler);

    const source_dir = this.options.source_dir;
    const filename = this.options.filename;

    compiler.hooks.make.tap(PLUGIN_ID, () => {
      if (!this.needs_compilation) {
        return;
      }

      const {list, imports} = this.getContent(source_dir);

      let content = '';
      content += `const index = {};${imports}\n`;
      content += `const list = ${JSON.stringify(list)};\n`;
      content += `export { list, index };`;

      // Add the dynamic file
      virtual_modules.writeModule(filename, content);

      this.needs_compilation = false;
    });

    compiler.hooks.watchRun.tap(PLUGIN_ID, () => {
      compiler.hooks.compilation.tap(PLUGIN_ID, (compilation) => {
        if (this.needs_compilation) {
          return;
        }

        compilation.hooks.buildModule.tap(PLUGIN_ID, (module) => {
          if (this.needs_compilation) {
            return;
          }

          if (module instanceof NormalModule) {
            const module_path = module.resource;
            if (module_path.startsWith(source_dir)) {
              this.needs_compilation = true;
            }
          }
        });
      });
    });
  }

  /**
   * Get list and imports to populate the dynamic file
   *
   * @param {string} dir The examples directory to walk
   * @returns {object} An object with 'list' and 'imports'
   */
  getContent(dir) {
    const filename = this.options.filename;
    const source_dir = this.options.source_dir;
    const base_path = this.options.base_path;

    const list = {};
    let imports = '';

    fs.readdirSync(dir).forEach((file) => {
      const filepath = path.join(dir, file);

      if (fs.statSync(filepath).isDirectory()) {
        if (fs.existsSync(path.join(filepath, "script.js"))) {
          const relative_path = path.join(base_path, path.relative(source_dir, filepath));
          list[file] = {
            'url': this.toPosix(path.join(relative_path, "script.js"))
          };

          if (fs.existsSync(path.join(filepath, "screenshot.jpg"))) {
            list[file].screenshot = this.toPosix(path.join(relative_path, "screenshot.jpg"));
          }

          let import_path = path.relative(path.dirname(filename), filepath);
          import_path = path.join(import_path, "script.js");
          import_path = this.toPosix(import_path);

          imports += `import * as ${file} from "./${import_path}";\n`;
          imports += `index.${file} = ${file};\n`;
        }
        else {
          const {list: sub_list, imports: sub_imports} = this.getContent(filepath, base_path);
          list[file] = sub_list;
          imports += sub_imports;
        }
      }
    });

    if (Object.keys(list).length === 0) {
      return null;
    }

    return {list, imports};
  }

  /**
   * Normalize a path to use POSIX separators
   *
   * @param {string} filepath The path to normalize
   * @returns {string} The normalized path
   */
  toPosix(filepath) {
    return filepath.split(path.sep).join(path.posix.sep);
  }
};
