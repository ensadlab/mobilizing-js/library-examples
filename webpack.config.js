const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const ExamplesListPlugin = require('./webpack/plugins/ExamplesList');


module.exports = (env, argv) => {
    return [{
        mode: 'production',
        bail: !argv.watch,
        devtool: 'source-map',
        entry: {
            'index': './src/index.js'
        },
        output: {
            filename: 'js/[name].js',
            path: path.resolve(__dirname, './dist'),
            libraryTarget: 'umd',
        },
        module: {
            rules: [
                {
                    // Process JS files.
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/preset-env'],
                            }
                        },
                    ]
                },
                {
                    // Process CSS files.
                    test: /\.css$/i,
                    use: ['style-loader', 'css-loader'],
                },
            ]
        },
        plugins: [
            new CleanWebpackPlugin(),
            new CopyPlugin({
                patterns: [
                    {
                        from: path.resolve(__dirname, './src/static'),
                        info: () => ({
                            minimized: true, // Keep white-space
                        }),
                    }
                ]
            }),
            new ExamplesListPlugin({
                filename: path.resolve(__dirname, './src/examples-list.js'),
                source_dir: path.resolve(__dirname, './src/static/examples'),
                base_path: 'examples',
            }),
        ],
    }];
};
